package be.pdn.training.aspectj.spring.boot.ltw;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.weaving.AspectJWeavingEnabler;

@SpringBootTest(classes = { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class })
class PresidentServiceTest {

  @Autowired
  ApplicationContext context;

  @Test
  void test() {
    var presidentService = context.getBean(PresidentService.class);
    presidentService.createPresident(new President(null, "Bill", "Clinton"));

    var president = presidentService.getPresident(1);
    assertAll(
        () -> assertEquals("Bill", president.getFirstname()),
        () -> assertEquals("Clinton", president.getLastname()));
  }

}
