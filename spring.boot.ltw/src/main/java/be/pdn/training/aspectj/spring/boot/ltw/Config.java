package be.pdn.training.aspectj.spring.boot.ltw;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.JdbcConnectionDetails;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizer;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
@EnableJpaRepositories(enableDefaultTransactions = false)
public class Config {

  @Bean
  TransactionManagerCustomizer<JpaTransactionManager> ptmCustomizer() {
    return ptm -> ptm.setValidateExistingTransaction(true);
  }

  @Bean
  JdbcConnectionDetails jdbcConnectionDetails() {
    return new JdbcConnectionDetails() {

      @Override
      public String getUsername() {
        return "SA";
      }

      @Override
      public String getPassword() {
        return "";
      }

      @Override
      public String getJdbcUrl() {
        return "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE";
      }

    };
  }

}
