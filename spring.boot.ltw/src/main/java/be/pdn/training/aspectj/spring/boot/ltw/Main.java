package be.pdn.training.aspectj.spring.boot.ltw;

import org.springframework.boot.SpringApplication;
import org.springframework.context.weaving.AspectJWeavingEnabler;

public class Main {

  public static void main(String[] args) {
    var ctx = SpringApplication.run(new Class[] { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class },
        args);
    var presidentService = ctx.getBean(PresidentService.class);
    presidentService.createPresident(new President(null, "Bill", "Clinton"));
    var president = presidentService.getPresident(1);
    System.out.println(president);
  }

}
