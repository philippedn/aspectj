package be.pdn.training.aspectj.plain.ltw;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LogAspect {

  @Pointcut("@annotation(Loggable) && execution(* *(..))")
  void logPointcut() {

  }

  @Before("logPointcut()")
  public void logBefore(JoinPoint jp) {
    System.out.format("Entering %s.%s()%n", jp.getTarget().getClass().getSimpleName(), jp.getSignature().getName());
  }

}
