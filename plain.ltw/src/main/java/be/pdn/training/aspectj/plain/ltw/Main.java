package be.pdn.training.aspectj.plain.ltw;

public class Main {

  public static void main(String[] args) {
    var service = new ExampleService();
    service.method1();

    service.setAttrib(5);
    System.out.println(service.getAttrib());
  }

}
