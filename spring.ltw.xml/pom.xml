<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>be.pdn.training.aspectj</groupId>
    <artifactId>parent</artifactId>
    <version>1.0.0</version>
  </parent>

  <artifactId>spring.ltw.xml</artifactId>

  <dependencies>
    <dependency>
      <groupId>org.aspectj</groupId>
      <artifactId>aspectjweaver</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-orm</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-beans</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.data</groupId>
      <artifactId>spring-data-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>jakarta.persistence</groupId>
      <artifactId>jakarta.persistence-api</artifactId>
    </dependency>

    <dependency>
      <!-- For @Transactional support, among other -->
      <groupId>org.springframework</groupId>
      <artifactId>spring-aspects</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.hibernate.orm</groupId>
      <artifactId>hibernate-core</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <!-- spring-data uses slf4j -->
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-jdk14</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <!-- Needed for instrumentation not to fail -->
      <groupId>com.querydsl</groupId>
      <artifactId>querydsl-jpa</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <!-- Needed for instrumentation not to fail -->
      <groupId>io.projectreactor</groupId>
      <artifactId>reactor-core</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <!-- Needed for instrumentation not to fail -->
      <groupId>org.jetbrains.kotlin</groupId>
      <artifactId>kotlin-reflect</artifactId>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-instrument</artifactId>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <!-- Execute with mvn dependency:properties exec:exec -->
        <configuration>
          <executable>${env.JAVA_HOME}/bin/java</executable>
          <arguments>
            <argument>-classpath</argument>
            <classpath />
            <argument>-javaagent:${org.springframework:spring-instrument:jar}</argument>
            <argument>--add-opens</argument>
            <argument>java.base/java.lang=ALL-UNNAMED</argument> <!-- https://github.com/eclipse-aspectj/aspectj/issues/117 -->
            <argument>be.pdn.training.aspectj.spring.ltw.Main</argument>
          </arguments>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <argLine>-javaagent:${org.springframework:spring-instrument:jar} --add-opens java.base/java.lang=ALL-UNNAMED</argLine>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <configuration>
          <ignoredUnusedDeclaredDependencies>
            <ignoredUnusedDeclaredDependency>org.junit.jupiter:junit-jupiter</ignoredUnusedDeclaredDependency>
            <ignoredUnusedDeclaredDependency>org.projectlombok:lombok</ignoredUnusedDeclaredDependency>
          </ignoredUnusedDeclaredDependencies>
        </configuration>
      </plugin>
    </plugins>
  </build>

</project>