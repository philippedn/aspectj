package be.pdn.training.aspectj.spring.ltw.xml;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ) // Instead of Spring based proxies
@EnableJpaRepositories(enableDefaultTransactions = false) // JPA repository methods will not be transactional (do this
                                                          // in the service layer instead)
@ComponentScan
public class Config {

  @Bean
  DataSource dataSource() {
    return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
  }

  @Bean
  PlatformTransactionManager transactionManager() {
    var transactionManager = new JpaTransactionManager(entityManagerFactory().getObject());
    transactionManager.setValidateExistingTransaction(true); // In case of nested @Transactional methods, check they are
                                                             // compatible (e.g. a read-only @Transactional calling
                                                             // another method with @Transactional should also be
                                                             // read-only)
    return transactionManager;
  }

  @Bean
  static PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  @Bean
  LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    var entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactoryBean.setDataSource(dataSource());
    entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    entityManagerFactoryBean.setPackagesToScan("be.pdn.training.aspectj.spring.ltw.xml");
    Properties jpaProperties = new Properties();
    jpaProperties.put("jakarta.persistence.schema-generation.database.action", "create");
    entityManagerFactoryBean.setJpaProperties(jpaProperties);
    return entityManagerFactoryBean;
  }

}
