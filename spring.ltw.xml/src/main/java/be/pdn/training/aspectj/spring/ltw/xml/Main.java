package be.pdn.training.aspectj.spring.ltw.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

  public static void main(String[] args) throws Exception {
    try (var context = new ClassPathXmlApplicationContext("application-config.xml")) {
      var presidentService = context.getBean(PresidentService.class);
      presidentService.addPresident(new President(null, "Bill", "Clinton"));
      var president = presidentService.getPresident(1);
      System.out.println(president);
    }
  }
}
