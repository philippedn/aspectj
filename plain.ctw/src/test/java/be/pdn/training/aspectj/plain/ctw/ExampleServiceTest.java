package be.pdn.training.aspectj.plain.ctw;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class ExampleServiceTest {

  @Test
  void test() {
    var byteArrayOutputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(byteArrayOutputStream));
    var service = new ExampleService();
    service.method1();
    var result = byteArrayOutputStream.toString();
    var expected = """
        Entering ExampleService.method1()%n\
        In method1()%n\
        Entering ExampleService.method2()%n\
        In method2()%n\
        """.formatted();
    assertEquals(expected, result);
  }

}
