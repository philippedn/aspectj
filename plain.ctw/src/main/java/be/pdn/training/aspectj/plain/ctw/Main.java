package be.pdn.training.aspectj.plain.ctw;

public class Main {

  public static void main(String[] args) {
    var service = new ExampleService();
    service.method1();
  }

}
