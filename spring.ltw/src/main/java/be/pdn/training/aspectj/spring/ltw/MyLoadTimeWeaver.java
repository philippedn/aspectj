package be.pdn.training.aspectj.spring.ltw;

import static org.springframework.context.ConfigurableApplicationContext.LOAD_TIME_WEAVER_BEAN_NAME;

import org.springframework.context.weaving.DefaultContextLoadTimeWeaver;
import org.springframework.stereotype.Component;

@Component(LOAD_TIME_WEAVER_BEAN_NAME)
public class MyLoadTimeWeaver extends DefaultContextLoadTimeWeaver {

}
