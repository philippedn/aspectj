package be.pdn.training.aspectj.spring.ltw;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.weaving.AspectJWeavingEnabler;

public class Main {

  public static void main(String[] args) throws Exception {
    try (var context = new AnnotationConfigApplicationContext(Config.class, AspectJWeavingEnabler.class,
        MyLoadTimeWeaver.class)) {
      var presidentService = context.getBean(PresidentService.class);
      presidentService.addPresident(new President(null, "Bill", "Clinton"));
      var president = presidentService.getPresident(1);
      System.out.println(president);
    }
  }
}
