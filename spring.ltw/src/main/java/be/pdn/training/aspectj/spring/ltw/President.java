package be.pdn.training.aspectj.spring.ltw;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class President {
  @Id
  @GeneratedValue
  @Setter(value = AccessLevel.NONE)
  private Long id;
  private String firstname;
  private String lastname;
}
