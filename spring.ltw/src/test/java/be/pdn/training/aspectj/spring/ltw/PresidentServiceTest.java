package be.pdn.training.aspectj.spring.ltw;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.weaving.AspectJWeavingEnabler;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class })
class PresidentServiceTest {

  @Autowired
  ApplicationContext context;

  @Test
  void test() {
    var presidentService = context.getBean(PresidentService.class);
    presidentService.addPresident(new President(null, "Bill", "Clinton"));

    var president = presidentService.getPresident(1);
    assertAll(
        () -> assertEquals("Bill", president.getFirstname()),
        () -> assertEquals("Clinton", president.getLastname()));
  }

}
