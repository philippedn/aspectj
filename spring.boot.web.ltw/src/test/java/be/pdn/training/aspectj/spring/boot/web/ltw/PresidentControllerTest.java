package be.pdn.training.aspectj.spring.boot.web.ltw;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.weaving.AspectJWeavingEnabler;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class })
@AutoConfigureMockMvc
class PresidentControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  void test() throws Exception {
    String presidentToCreate = """
        {
          "firstname":"Bill",
          "lastname":"Clinton"
        }
        """;
    String presidentToExpect = """
        {
          "id":1,
          "firstname":"Bill",
          "lastname":"Clinton"
        }
        """;

    mockMvc.perform(
        post("/presidents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(presidentToCreate))
        .andExpectAll(
            status().isCreated(),
            content().json(presidentToExpect, true));

    mockMvc.perform(
        get("/presidents/1")
            .accept(MediaType.APPLICATION_JSON))
        .andExpectAll(
            status().isOk(),
            content().json(presidentToExpect, true));
  }

}
