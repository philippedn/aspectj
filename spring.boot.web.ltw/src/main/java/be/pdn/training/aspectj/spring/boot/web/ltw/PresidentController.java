package be.pdn.training.aspectj.spring.boot.web.ltw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/presidents")
public class PresidentController {

  @Autowired
  PresidentService presidentService;

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  President getPresident(@PathVariable("id") Long id) {
    return presidentService.getPresident(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  President createPresident(@RequestBody President president) {
    return presidentService.createPresident(president);
  }

}
