package be.pdn.training.aspectj.spring.boot.web.ltw;

import org.springframework.boot.SpringApplication;
import org.springframework.context.weaving.AspectJWeavingEnabler;

public class Main {

  public static void main(String[] args) {
    SpringApplication.run(new Class[] { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class }, args);
  }

}
