package be.pdn.training.aspectj.spring.web.ltw;

import org.springframework.context.weaving.AspectJWeavingEnabler;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Main extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return null;
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[] { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/*" };
  }

}