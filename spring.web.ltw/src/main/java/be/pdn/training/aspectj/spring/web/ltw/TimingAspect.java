package be.pdn.training.aspectj.spring.web.ltw;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclarePrecedence;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
@DeclarePrecedence("TimingAspect,*") // Before any other aspect
public class TimingAspect {

  @Pointcut("@annotation(Timing) && execution(* *(..))")
  void timingPointcut() {
  }

  @Around("timingPointcut()")
  public Object timingAdvice(ProceedingJoinPoint pjp) throws Throwable {
    long start = System.nanoTime();
    var result = pjp.proceed();
    long end = System.nanoTime();
    double time = (end - start) / 1_000_000.0;
    System.out.format("%s.%s(): %.3fms%n", pjp.getTarget().getClass().getSimpleName(), pjp.getSignature().getName(),
        time);
    return result;
  }
}
