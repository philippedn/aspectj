package be.pdn.training.aspectj.spring.web.ltw;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PresidentService {

  @Autowired
  PresidentRepository presidentRepository;

  @Timing
  @Transactional
  public President getPresident(long id) {
    return presidentRepository.findById(id).get();
  }

  @Timing
  @Transactional
  public President createPresident(President p) {
    return presidentRepository.save(p);
  }

  @Timing
  @Transactional
  public void deletePresident(long id) {
    presidentRepository.deleteById(id);
  }

  @Timing
  @Transactional
  public List<President> getAllPresidents() {
    return presidentRepository.findAll();
  }

}
