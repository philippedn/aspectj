package be.pdn.training.aspectj.spring.web.ltw;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PresidentController {

  @Autowired
  private PresidentService presidentService;

  @GetMapping("/presidents/{id}")
  President getPresident(@PathVariable Long id) throws IOException {
    return presidentService.getPresident(id);
  }

  @PostMapping("/presidents")
  @ResponseStatus(HttpStatus.CREATED)
  President createPresident(@RequestBody President president) {
    return presidentService.createPresident(president);
  }

  @GetMapping("/presidents")
  List<President> getPresidents() {
    return presidentService.getAllPresidents();
  }

}
