package be.pdn.training.aspectj.spring.web.ltw;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.weaving.AspectJWeavingEnabler;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { Config.class, AspectJWeavingEnabler.class, MyLoadTimeWeaver.class })
@WebAppConfiguration
class PresidentControllerTest {

  @Autowired
  WebApplicationContext wac;

  @Test
  void test() throws Exception {
    MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    mockMvc.perform(
        post("/presidents").contentType(MediaType.APPLICATION_JSON).content("""
            {"firstname":"Bill","lastname":"Clinton"}"""))
        .andExpectAll(
            status().isCreated(), content().string("""
                {"id":1,"firstname":"Bill","lastname":"Clinton"}"""));

    mockMvc.perform(
        get("/presidents/1").accept(MediaType.APPLICATION_JSON))
        .andExpectAll(
            status().isOk(), content().string("""
                {"id":1,"firstname":"Bill","lastname":"Clinton"}"""));

  }

}
