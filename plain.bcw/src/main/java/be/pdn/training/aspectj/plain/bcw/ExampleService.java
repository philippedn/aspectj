package be.pdn.training.aspectj.plain.bcw;

import lombok.Data;

@Data
public class ExampleService {

  private int attrib;

  @Loggable
  public void method1() {
    System.out.println("In method1()");
    method2();
  }

  @Loggable
  public void method2() {
    System.out.println("In method2()");
  }

}
