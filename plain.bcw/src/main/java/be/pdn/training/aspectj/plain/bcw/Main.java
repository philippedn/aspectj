package be.pdn.training.aspectj.plain.bcw;

public class Main {

  public static void main(String[] args) {
    var service = new ExampleService();
    service.method1();

    service.setAttrib(5);
    System.out.println(service.getAttrib());
  }

}
